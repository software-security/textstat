use std::io::{BufRead, BufReader};

#[test]
fn test_analyze() {
    /*
    Feel free to add tests here. When you give analyze a return type
    then you can check the return value as well.
    */
    analyze(&mut String::from("test").as_bytes());
}

fn analyze(br: &mut dyn BufRead) {
    /*
    Complete your implementation here. You can use br to read your input.
    Feel free to change the return type of the function as well.
    */
}

fn main()  {
    // Get the arguments that were passed to the function
    let args: Vec<_> = std::env::args().collect();
    
    // Check whether we have exactly two arguments
    if args.len() != 2 {
        println!("Usage: {} <filename>", args[0]);
        std::process::exit(1);
    }

    // Open the input file
    let filename = &args[1];
    let fh = std::fs::File::open(filename).unwrap_or_else(|err| {
        println!("Failed to open file {}: {}", filename, err);
        std::process::exit(2);
    });

    // Generate a BufReader from the file and pass it to analyze
    let mut br = BufReader::new(fh);
    analyze(&mut br);
}
